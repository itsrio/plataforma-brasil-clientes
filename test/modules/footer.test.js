/*jshint expr: true */
/*global describe, it */

var should = require('should');
var footer = require('../../lib/modules/footer');

describe('footer', function () {

  describe('controller', function () {

    it('should be a function', function () {
      footer.controller.should.be.a.function;
    });
/*
    it('should return an object', function () {
      footer.controller().should.be.a.object;
    });
*/
  });

  describe('view', function () {

    it('should be a function', function () {
      footer.view.should.be.a.function;
    });
/*
    it('should return an object', function () {
      footer.view().should.be.a.object;
    });
*/
  });

});
