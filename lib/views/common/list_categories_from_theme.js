var m = require('mithril');

var list_categories = function (ctrl, oncheck) {
	var current_category = m.route.param('categoria');
	var cat = ctrl.theme_active().categories.map(function (category) {
		var selected = (category.slug === current_category ? '.selected' : '');
		return m('.category-option', [
				m('a.pure-button.button-small'+selected+'[href=#][data-slug='+category.slug+'][data-id='+category.id+'][data-value='+category.name+']', 
					{ onclick: oncheck.bind(this, ctrl) },
					category.name 
				)
			]);
	});
	return cat;
};

module.exports = list_categories;
