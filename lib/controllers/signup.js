var m = require('mithril');
var phaseModel = require('models/phase');
var c = require('config');
var userModel = require('models/user');
var Auth = require('models/auth');
var headerM = require('controllers/header');
var _ = require('underscore');
var Cookies = require('cookies-js');

var choose_tag = require('views/signup/choose_tag');
var main_form = require('views/signup/form');

/**
 ** 0. fazer nada se o usuário caiu na tela diretamente
 * 1. verificar se access_token existe na url, se existir pedir dados na api
 * 2. se o usuário já está cadastrado, logar com token
 * 3. se o usuário não cadastrado, preencher infos no formulário
 * 4. clicou em salvar, tentar cadastrar usuário
 * 5. se cadastrou com sucesso, logar com token
 */
var SignUp = module.exports = {
    user: m.prop(localStorage.user),

    controller: function() {
        var ctrl = this;
        this.user = SignUp.user;
        this.caracteresRestantes = m.prop("0");
        ctrl.error = m.prop('');

        this.setCookies = function(user) {
            var cookie_defaults = { domain : '.' + document.domain, expires: Infinity };
            Cookies.set('logged_in', 'true', cookie_defaults);
            Cookies.set('user_email', user.email, cookie_defaults);
            Cookies.set('user_photourl', user.photo_url, cookie_defaults);
            Cookies.set('user_setor', user.sector, cookie_defaults);
            Cookies.set('user_nome', user.nome, cookie_defaults);
        };

		this.redirect_after_login = function () {
            
			if (m.route.param("back_to")) {
                var back_to = m.route.param("back_to");
                if (back_to.indexOf('blog')) {
                    window.location = back_to;
                } else {
                    m.route(back_to);
                }
			} else {
				if (phaseModel.getCurrentActive() == 1) {
					m.route('/fase-1-votar');
				} else {
					m.route("/fase-2-participar");
				}
			}
		};

        this.isNewRecord = true;
        this.vm = SignUp.vm;
        this.vm.facebookSignup = false;
        this.vm.popupTagOpened = false;
        if (userModel.setFacebookCode()) {
            userModel.getFacebookData().then(function(data) {
                if ("token" in data) {
                    userModel.saveLogin(data)
                        .then(function(data) {
                            ga('set', '&uid', data.Email);
                            ctrl.setCookies(data);
							ctrl.redirect_after_login();
                            m.module(document.getElementById('header'), headerM);
                        });
                } else if ("ID" in data) {
                    SignUp.vm.facebookSignup = true;
                    var gender = data.Gender;
                    if (gender == 'male') {
                        gender = 'Masculino';
                    }
                    if (gender == 'female') {
                        gender = 'Feminino';
                    }

                    SignUp.vm.nome(data.Name);
                    SignUp.vm.photo_url('https://graph.facebook.com/' + data.ID + '/picture/');
                    SignUp.vm.facebook_id(data.ID);
                    SignUp.vm.email(data.Email);
                    SignUp.vm.genero(gender);
                }
            });
        } else if ((localStorage.pre_sign_up !== "") && (typeof localStorage.pre_sign_up !== 'undefined')) {
            var data = JSON.parse(localStorage.pre_sign_up);

            SignUp.vm.nome(data.nome);
            SignUp.vm.email(data.email);
            SignUp.vm.cpf(data.cpf);
            SignUp.vm.password(data.password);
        } else {
            m.route('/entrar');
        }

        m.request({
            method: 'GET',
            url: './json/states.json',
            background: true
        }).then(function(data) {
            data.unshift({
                id: '',
                name: 'Estado'
            });
            SignUp.vm.states(data);
            SignUp.vm.statesDisabled = false;
        });

        this.trocarFoto = function(ctrl) {
            var deferred = m.deferred();
            var pic = ctrl.vm.profilePicture();
            if (pic && pic[0]) {
                var reader = new FileReader();
                reader.onload = function(ee) {
                    ctrl.vm.photo_url(ee.target.result);
                    m.redraw(true); // force

                    var formData = new FormData();
                    formData.append("data", pic[0]);

                    Auth.req({
                        method: "POST",
                        url: c.getApiHost() + "/users/avatar",
                        data: formData,
                        serialize: function(value) {
                            return value;
                        }
                    }).then(function(image_uploaded) {
                        var u = JSON.parse(localStorage.user);
                        u.photo_url = image_uploaded.photo_url;
                        localStorage.user = JSON.stringify(u);
                        deferred.resolve(image_uploaded);
                    });
                };
                reader.readAsDataURL(pic[0]);
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        };

        this.contaCaracteres = function(value, e) {
            ctrl.caracteresRestantes(500 - parseInt(_(e.target.value).size()));
        };

        this.save = function(e) {
            e.preventDefault();
            var ctrl = this;

            if (this.vm.sector() === "") {
                var message = 'É necessário selecionar o setor da sociedade.';
                ctrl.error(m(".alert-error", message));
                return;
            }

            var u = {
                nome: ctrl.vm.nome(),
                photo_url: ctrl.vm.photo_url(),
                email: ctrl.vm.email(),
                genero: ctrl.vm.genero(),
                facebook_id: ctrl.vm.facebook_id(),
                sector: ctrl.vm.sector(),

                cpf: ctrl.vm.cpf(),

                password: ctrl.vm.password(),
                data_nascimento: e.target.data_nascimento.value,
                ocupacao: e.target.ocupacao.value,
                cidade: e.target.cidade.value,
                estado: e.target.estado.value,
                descricao: e.target.descricao.value
            };

            userModel.saveUser(u)
                .then(function(data) {
                    ctrl.setCookies(data);
                    if (data && data.token) {
                        userModel.saveLogin(data)
                            .then(function() {
                                return ctrl.trocarFoto(ctrl);
                            })
                            .then(function() {
                                m.module(document.getElementById('header'), headerM);
                                localStorage.pre_sign_up = '';
                                ctrl.redirect_after_login();
                            });
                    } else {
                        var message = 'Houve um erro na criação da conta';
                        ctrl.error(m(".alert-error", message));
                    }
                }, function(err) {
                    var message = 'Houve um erro na criação da conta';
                    ctrl.error(m(".alert-error", err.message));
                });
        };
    },

    vm: {
        nome: m.prop(""),
        photo_url: m.prop("img/unknown.jpg"),
        facebook_id: m.prop(""),
        email: m.prop(""),
        cidade: m.prop(""),
        estado: m.prop(""),
        descricao: m.prop(""),
        ocupacao: m.prop(""),
        data_nascimento: m.prop(""),
        genero: m.prop(""),
        sector: m.prop(""),
        cpf: m.prop(""),
        password: m.prop(""),
        sectorPopup: m.prop("br"),
        sectorDescriptionPopup: m.prop("Quero colaborar no site como cidadão e não me sinto parte de nenhum dos outros setores listados."),

        profilePicture: m.prop(""),

        fecharTagPopup: function() {
            SignUp.vm.popupTagOpened = false;
        },

        openPopupClick: function(ctrl, e) {
            var msg = {
                'gov': 'Meu trabalho é em algum ramo do Estado, seja ele o executivo, judiciário ou legislativo, em qualquer esfera da federação (União, Estados e Municípios).',
                'edu': 'Sou professor, pesquisador ou alguém que trabalha no meio acadêmico com produção de conhecimento, ensino ou pesquisa.',
                'ong': 'Trabalho em organizações da sociedade civil, em redes descentralizadas, em ONGs, movimentos sociais, sindicatos e qualquer outra entidade do terceiro setor.',
                'com': 'Trabalho no setor privado.',
                'br': 'Quero colaborar no site como cidadão e não me sinto parte de nenhum dos outros setores listados.'
            };
            var s = e.target.id;
            this.vm.sectorPopup(s);
            this.vm.sectorDescriptionPopup(msg[s]);
            this.vm.popupTagOpened = true;
        },

        sectorClick: function(sector) {
            SignUp.vm.popupTagOpened = false;
            SignUp.vm.sector(sector);
            [].forEach.call(document.getElementsByClassName('sector'), function(e) {
                e.classList.remove('active');
            });
            setTimeout(function() {
                document.getElementById(sector).classList.add('active');
            }, 500);
        },
        states: m.prop([{
            id: '',
            name: 'Carregando'
        }]),
        selectedState: m.prop(""),
        selectedCitie: m.prop(""),
        statesDisabled: true,
        cities: m.prop(['Escolha um estado antes']),
        citiesDisabled: true,
        changeState: function(state) {
            m.request({
                method: 'GET',
                url: './json/' + state.toLowerCase() + '.json'
            }).then(function(data) {
                data.unshift('Cidade');
                SignUp.vm.cities(data);
                SignUp.vm.citiesDisabled = false;
            });
        }
    },

    view: function(ctrl) {
        return m('.signup',
            m('.container', [
                m('.pure-g', [
                    m('.pure-u-1', [
                        m('p', 'Cadastre-se e participe da Plataforma Brasil!'),
                        m('p', 'A construção acontece de forma colaborativa e os participantes querem saber quem é você. Todos poderão ver sua foto, de que setor da sociedade você pertence e suas ideias e contribuições. Para isso, preencha os dados abaixo:')
                    ]),
                    m('.pure-u-1', [
                        m('h2.section-title', 'escolha o setor da sociedade com o qual se identifica'),
                        m('.baloon-bg.yellow'),
                    ]),
                ]),

                m('form.pure-form[name=signup]', {
                    onsubmit: ctrl.save.bind(ctrl)
                }, [

                    choose_tag(ctrl),

                    m('.pure-g',
                        m('.pure-u-1', [
                            m('h2.section-title', 'preencha seus dados'),
                            m('.baloon-bg.yellow'),
                        ])
                     ),

                    main_form(ctrl),

                    m('.pure-g', [
                        m('.pure-u-1',
                            m('label.agree[for=privacidade]', [
                                m('input[type=checkbox][name=privacidade][id=privacidade]', {
                                    required: true
                                }),
                                'Li e concordo com a ',
                                m('a[href=/?/politica-de-privacidade][target=_blank]', 'Política de privacidade')
                            ])
                        ),
                        m('.pure-u-1',
                            m('label.agree[for=termos]', [
                                m('input[type=checkbox][name=termos][id=termos]', {
                                    required: true
                                }),
                                'Li e concordo com os ',
                                 m('a[href=/?/termos-de-uso][target=_blank]', 'Termos de uso')
                             ])
                        ),
                    ]),
                    m('button.pure-button', {
                        type: "submit"
                    }, 'Salvar')
                ])
            ])
        );
    }
};
