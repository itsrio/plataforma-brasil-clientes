var m = require('mithril');

var badge_user = function (perfil, size) {
	var avatar = perfil.photo_url;

	if (avatar.match(/graph/gi) !== null) {
		avatar = avatar.replace("?type=large","");
		avatar = avatar + '?width=' + size + '&height=' + size;
	}
	//avatar = 'http://proxy.boxresizer.com/convert?resize='+size+'x'+size+'&shape=pad&source='+avatar;

	return [
		m('a.profile-tag.'+perfil.sector+'.profile-size-'+size+'[href=/perfil/'+perfil.id+']', {config:m.route}, [
			m('img.pure-img.svg[src='+avatar+'][width=' + size + ']'
			),
			m('span.profile-desc',perfil.sector)
		])
	];
};

module.exports = badge_user;
