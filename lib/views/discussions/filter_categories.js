var m = require('mithril');
var list_categories = require('views/common/list_categories_from_theme');
var filter_categories = function (ctrl) {
	return [
		m('.filter-categories', [
			m('p', 'filtre por categoria'),
			list_categories(ctrl, ctrl.setCategory),
	])];
};

module.exports = filter_categories;
