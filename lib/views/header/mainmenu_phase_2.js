var m = require('mithril');

var submenu = function(label,description,url) {
    return [
        m('li.pure-menu-item',[
            m('a.pure-menu-link[href='+url+']', {config: m.route},[
                m('span.mark', label),
                m('span.mark-small', description)
            ])
        ])
    ];
};

var mainmenu = function (ctrl) {
    return [
        // m('.pure-menu',[
            m('ul.main-menu.pure-menu-list.fix-menu', [
                m('li.pure-menu-item', [
                    m('a.menu-item[href=/fase-1-resultados]', {config: m.route},[
                        m('i.icon-resultados'),
                        m('span.small', 'resultados da'),
                        m('span.big', 'Priorização')
                    ])
                ]),
                m('li.pure-menu-item.pure-menu-has-children.pure-menu-allow-hover', [
                    m('a.menu-item[href=/]', {config: m.route},[
                        m('i.icon-discussao'),
                        m('span.small', 'Veja os'),
                        m('span.big', 'Temas')
                    ]),
                    m('ol.pure-menu-children.hidden-menu-sm', function () {
                        var options = [];
                        ctrl.themes().forEach(function (data) {
                            options.push(submenu(("0" + data.position).substr(-2,2),data.name,'/fase-2-discutir-tema/'+data.id));
                        });
                        return options;
                    }())
                ])
            ])
        // ])
    ];
};
module.exports = mainmenu;