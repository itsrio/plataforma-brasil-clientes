var m = require('mithril');
var baloon_bottom = require('views/common/baloon_bottom');
var choose_themes = require('views/home/phase_2_choose_theme'); 

//box_menu removed by QA Jun-19
var box_menu = function (name) {
	return [
		m('.pure-g',
			m('.pure-u-1.pure-u-md-11-24.box-challenge',[
				m('a[href=/fase-1-resultados]', { config: m.route }, [
					m('.pure-u-2-5.featured',[
						m('img.svg[src=img/fase-2/calendar-1.svg]'),
						m('img.svg[src=img/fase-2/calendar-1-1.svg]')
					]),
					m('.pure-u-3-5',[
						m('h3','desafios priorizados'),
						m('.cta',[
							m('i.icon-resultados'),
							'veja como foi'
						])
					])
				])
			]),
			m('.pure-u-1.pure-u-md-11-24.box-challenge',[
				m('a[href=/fase-2-participar]', { config: m.route }, [
					m('.pure-u-2-5.featured',[
						m('img.svg[src=img/fase-2/calendar-2.svg]'),
						m('img.svg[src=img/fase-2/calendar-3.svg]'),
					]),
					m('.pure-u-3-5',[
						m('h3','Construiremos soluções em conjunto'),
						m('.cta',[
							m('i.icon-discussao'),
							'venha participar'
						])
					])
				])
			])
	)];
};

// http://accordionslider.com/
var box_challenge = function (ctrl) {
	return [
		// box_menu(),
		m('h3.title', 'Dê sua resposta para cada uma dessas cinco perguntas:'),
		choose_themes(ctrl)
	];
};

module.exports = box_challenge;
