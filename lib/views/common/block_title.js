var m = require('mithril');

module.exports = function (ctrl,title) {
    return [
		m('.block-title',[
            m('h2.block-title-name', title)
        ])
	];
};