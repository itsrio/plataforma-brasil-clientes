var m = require('mithril');

var phaseModel = require('models/phase');
var phase_1 = require('views/header/loggedin_phase_1');
var phase_2 = require('views/header/loggedin_phase_2');
var phase_3 = require('views/header/loggedin_phase_3');

var badge_user = require('views/common/badge_user');

var loggedin = function(ctrl) {
    var nome = ctrl.vm.user().nome;
    nome = nome.split(" ");
    var primeiro_nome = nome[0];

	var loggedin_submenu = {};
	if (phaseModel.getCurrentActive() == 1) {
		loggedin_submenu = phase_1(ctrl);
	} else if (phaseModel.getCurrentActive() == 2) {
		loggedin_submenu = phase_2(ctrl);
	} else if (phaseModel.getCurrentActive() == 3) {
		loggedin_submenu = phase_3(ctrl);
	}

    return [
        m('ul.main-menu.pure-menu-list.fix-menu.loggedin', [
            m ('li',[
                (ctrl.vm.total_notifications() > 0 ? m('a.profile-notifications[href=/notificacoes]', { config: m.route },
                    m('i.icon-notificacao'),
                    m('span.count',ctrl.vm.total_notifications())
                ) : ''),
                badge_user(ctrl.vm.user(), 50)
             ]),
             m('li.pure-menu-item.menu-mark-arrow.pure-menu-has-children.pure-menu-allow-hover',[
                 m('a.username[href=/meu-perfil]',
                     { config: m.route },
                    primeiro_nome,
                	m('i.pure-hidden-sm.icon-chevron-down.'+ ctrl.vm.user().sector)
                ),
                m('ol.pure-menu-children.pure-hidden-sm.hidden-menu-sm', loggedin_submenu)
            ])
        ]),
        m('ul.pure-u-1.submenu-mobile.main-menu.pure-menu-list.fix-menu.pure-visible-only-sm', loggedin_submenu)
    ];
};

module.exports = loggedin;
