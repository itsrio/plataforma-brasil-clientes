var m = require('mithril');
var baloon_bottom = require('views/common/baloon_bottom');
var feature_box = function(title, subtitle) {
    return [
            m('.pure-u-sm-5-24.pure-hidden-sm'),
            m('.featured-box.pure-u-1.pure-u-sm-14-24', [
                title,
                m('.divider'),
                subtitle,
                baloon_bottom()
            ])
    ];
};

module.exports = feature_box;

