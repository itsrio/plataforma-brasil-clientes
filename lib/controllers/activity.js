var m = require('mithril');

function controller() {
    return {};
}

function view() {
    return m('div', 'activity');
}

module.exports = {
    controller: controller,
    view: view
};
