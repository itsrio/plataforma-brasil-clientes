var m = require('mithril');

var mainmenu = function(ctrl) {
	return [
		m('.action-menu-mobile.pure-visible-only-sm.pure-u-1-4', [
		    m('a[href=#]', {onclick: ctrl.toggleMenu}, [
		        m('i.icon-menu')
		    ])
		]),
		m('.logo.pure-u-1-2.pure-u-sm-4-24', [
		    m('a[href=/]', { config: m.route }, [
		        m('img.svg.pure-img[src="img/logo-h.svg"]')
		    ]),
		]),
		m('.menu-notification.pure-u-1-4.pure-visible-only-sm')
	];
};

module.exports = mainmenu;