var m = require('mithril');
var baloon_bottom = require('views/common/baloon_bottom');

module.exports = function (ctrl,title,image) {
    return [
        m('.hero-report', [
            m('.container', [
                m('.ilustration.pure-u-sm-1-3', [
                    m('img[src=/'+image+']'),
                ]),
                m('.featured.pure-u-sm-2-3', [
                    m('h2', title),
                    baloon_bottom()
                ])
            ])
        ])
    ];
};