var m = require('mithril');
var creditos_foto = require('views/common/creditos_foto');
var configModel = require('models/config');
var md2html = require('markdown2mithril');

var ombudsman = {};

ombudsman.controller = function () {
    var desc = m.prop('');
    configModel.get_site('page_critica').then(function(data) {
        desc(data);  
    });

    return {
        content : function () {
            return md2html(desc());
        }
    };
};

ombudsman.view = function (ctrl) {
    return m('.page', [
        m('.hero', [
            m('.title.pure-u-4-5.pure-u-sm-3-5', [
                m('h1', 'críticas e sugestões')
            ]),
            creditos_foto()
        ]),
        m('.container', [
            m('.pure-g.content', [
                m('.pure-u-1.pure-u-sm-1-2', [
                    m('img.pure-img[src=img/baloon-ombudsman.svg]')
                ]),
                m('.pure-u-1.pure-u-sm-1-2', [
                    m('.pure-u-1-5', m('.separator')),
                    m('.pure-u-4-5', [
                        m('h2.subtitle', 'queremos ouvir você'),
                        m('h2', 'o que você tem a dizer?')
                    ]),
                    m('ul.perguntas', [
                        m('li', m('a[href=#pergunta-1]', 'Qual o papel do ombudsman na Plataforma Brasil?')),
                        m('li', m('a[href=#pergunta-2]', 'Como o ombudsman pode ajudar?')),
                        m('li', m('a[href=#pergunta-3]', 'Como acionar o ombudsman?')),
                        m('li', m('a[href=#pergunta-4]', 'Como acompanhar o trabalho do Ombudsman?')),
                        m('li', m('a[href=#quem-e]', 'Quem é o Ombudsman?'))
                    ])
                ]),
                m('.pure-u-1', [
                    ctrl.content(),
                ])
            ])
        ])
    ]);
};

module.exports = ombudsman;
