var m = require('mithril');
var badge_user = require('views/common/badge_user');
var moment = require('moment');
var social_actions = require('controllers/common/social_actions');
var md2html = require('markdown2mithril');

var combo_discussion = function (data, page_name, show_user_info, show_actions, show_social_actions, show_categories) {
	// var column_size = show_user_info ? ".pure-u-4-5" : ".pure-u-1";
	var ctrl_sa = new social_actions.controller(); 
	ctrl_sa.data = m.prop(data);
	ctrl_sa.content_tip = m.prop(false);

	var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	var regex = new RegExp(expression);
	var t = data.description.replace(regex, '[$&]($&)');
	if (t) {
		data.description = t;
	}


	if (!show_user_info) {
		data.user = {};
		data.user.sector = '';
	}
	var list_categories = [];
	if ( (data.categories !== undefined) && (data.categories !== null) && (show_categories) ) {
		data.categories.forEach(function (category, k) {
			list_categories.push(
				m('.category-option', [
					m('a.pure-button.button-small[href=#][id=c'+category.id+']', category.name ),
					m('input[type=checkbox][name=categories[]][value='+category.id+'][id=cc'+category.id+']')
				]));
		});
	}

	return [
		m('.comment-item.pure-g',[
			m('.post-background.pure-u-22-24',[
				( show_user_info ? 
					m('.profile-info.pure-u-sm-1-5.pure-u-1',[
						m('.pure-u-1-5.pure-u-sm-1-1.pure-hidden-sm',
							badge_user(data.user, 80)
						),
						m('.pure-u-1-5.pure-u-sm-1-1.pure-visible-only-sm',[
							(page_name == 'comments') ? badge_user(data.user, 50) : badge_user(data.user, 70),
						]),
						m('.pure-u-3-5.pure-u-sm-1-1.profile-name-space',
							m('h3.profile-name',data.user.nome)
						)
					])
				 : 
					m('.profile-info.pure-u-sm-1-24')
				),
				m('.box-post.'+data.user.sector+'.pure-u-sm-4-5.pure-u-1',[
					list_categories,
					m('h2.post-title', m('a[href=/fase-2-comentar-discussao/'+data.id+']', {config: m.route}, data.title)),
					m('p.post-created', moment(data.created_at).fromNow()),
					m('p.post-description',
					  	md2html(data.description)
//							m('a.read-more[href=#]',' ver mais ')
					),
					( show_actions ? 
						m('.actions',[
							// m('a.pure-button'+
							// (page_name === 'handyman' ? '.selected' : '')+
							// '[href=/fase-2-informar-se-discussao/'+data.id+']', {config: m.route}, [
							// 	m('i.icon-biblioteca'),
							// 	m('span','informe-se')
							// ]),
							m('a.pure-button'+
							  (page_name === 'comments' ? '.selected' : '')+
							  '[href=/fase-2-comentar-discussao/'+data.id+']', {config: m.route}, [
								m('i.icon-notificacao'),
								m('span',data.count_comments + ' Comentários')
							]),
						])
					: '')
				]),
			]),
			m('.social-options.pure-u-2-24', social_actions.view(ctrl_sa))
		])
	];
};
module.exports = combo_discussion;
