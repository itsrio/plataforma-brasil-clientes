var m = require('mithril');
var badge_user = require('views/common/badge_user');
var moment = require('moment');
var divisor = require('views/common/badge_divisor');

var combo_discussion = function (disc) {

	return [
		m('.comment-item.pure-g',[
			m('.pure-u-1-5.pure-hidden-sm',
				badge_user(disc.user, 50)
			),
	    	m('.comment-data.pure-u-sm-4-5.pure-u-1',[
	    		m('span.comment-profile-name',disc.user.nome),
	    		m('a.comment-text[href=/fase-2-comentar-discussao/'+disc.id+']',{config:m.route}, disc.description),
	    		m('span.comment-date', moment(disc.created_at).fromNow())
	    	])
		])
	];
};

var box_discussion = function (ctrl) {
	return [
	    m('.featured-discussion', [
	        m('.feature-text.pure-g', [
	        	divisor('icon-discussao','','Respostas em destaque'),
	    //         m('.bullet.pure-u-1-5', [
					// m('.position.separator'),
	    //             m('i.icon-discussao')
	    //         ]),
	    //         m('.title.pure-u-4-5', [
	    //             m('h1', 'Discussões em destaque')
	    //         ]),
	            m('.box-comments',[
	            	m('.comment-list.', ctrl.discussions().map(function (disc) {
	            		return combo_discussion(disc);
					})
				)
	        ])
	    ])
	])
	];
};

var module_discussion = function () {
    return [
        box_discussion()
    ];
};

module.exports = box_discussion;