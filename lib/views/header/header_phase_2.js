var m = require('mithril');
var _ = require('underscore');
var baloon_bottom = require('views/common/baloon_bottom');

var mainmenu = function(ctrl) {
	return [
		m('.action-menu-mobile.pure-visible-only-sm.pure-u-1-5', [
		    m('a[href=#]', {onclick: ctrl.toggleMenu}, [
		        m('i.icon-menu')
		    ])
		]),
		m('.logo.pure-u-3-5.pure-u-sm-1-5', [
		    m('a[href=/]', { config: m.route }, [
		        m('img.svg.pure-img[src="img/logo-h.svg"]')
		    ]),
		]),
	];
};

module.exports = mainmenu;
