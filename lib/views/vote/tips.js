var m = require('mithril');

var tips = function () {
    return m('.box-tips-fase-um', [
        m('.container', [
            m('.pure-g', [
                m('.pure-u-2-24.pure-hidden-sm'),
                m('.pure-u-1.pure-u-sm-20-24', [
                    m('h2.section-title', 'fase 01: priorizar temáticas'),
                    m('.baloon-bg.yellow'),
                    m('.pure-g', [
                        m('.pure-u-1.pure-u-sm-6-24.tip', [
                            m('img[src=img/illustration/dica-priorizar-escolha.svg]'),
                            m('p', 'Dos dois temas apresentados, clique no mais prioritário. Em seguida, repita o processo.')
                        ]),
                        m('.pure-u-1.pure-u-sm-6-24.tip', [
                            m('img[src=img/illustration/dica-priorizar-pular.svg]'),
                            m('p', 'Se ficar em dúvida, você pode pular as opções...')
                        ]),
                        m('.pure-u-1.pure-u-sm-6-24.tip', [
                            m('img[src=img/illustration/dica-priorizar-biblioteca.svg]'),
                            m('p', '...ou acessar a Civviki, um site com explicações sobre temas aqui abordados.')
                        ]),
                        m('.pure-u-1.pure-u-sm-6-24.tip', [
                            m('img[src=img/illustration/dica-priorizar-resultado.svg]'),
                            m('p', 'É possível priorizar quantas vezes desejar. Quando quiser, veja os resultados.')
                        ]),
                    ])
                ]),
            ])
        ]),
    ]);
};

module.exports = tips;
