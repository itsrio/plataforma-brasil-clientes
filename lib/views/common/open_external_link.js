var open_external_link = function (e) {
    e.preventDefault();
    var href = (e.target.href !== undefined ? e.target.href : this.href);
    var ref = window.open(href, '_blank');
};

module.exports = open_external_link;

