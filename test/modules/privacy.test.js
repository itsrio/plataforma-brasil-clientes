/*jshint expr: true */
/*global describe, it */

var should = require('should');
var privacy = require('../../lib/modules/privacy');

describe('privacy', function () {

  describe('controller', function () {

    it('should be a function', function () {
      privacy.controller.should.be.a.function;
    });

    it('should return an object', function () {
      privacy.controller().should.be.a.object;
    });

  });

  describe('view', function () {

    it('should be a function', function () {
      privacy.view.should.be.a.function;
    });

    it('should return an object', function () {
      privacy.view().should.be.a.object;
    });

  });

});
