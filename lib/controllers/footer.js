var m = require('mithril');
var moment = require('moment');
var topbar = require('views/header/topbar');
var footer = {};
var open_external_link = require('views/common/open_external_link');
footer.controller = function() {
    var ctrl = this;
    ctrl.posts = m.prop([]);
    m.request({
        method: 'GET',
        url: 'https://public-api.wordpress.com/rest/v1.1/sites/blog.plataformabrasil.org.br/posts/',
        background: true,
        unwrapSuccess: function(response) {
            return response.posts;
        }
    })
    .then(function(data) {
        ctrl.posts(data);
        m.redraw();
    });
};

footer.view = function(ctrl) {
    return m('.container', [
        m('.pure-g', [
            m('.pure-u-1.pure-u-sm-2-5.blog', [
                m('h2', 'Acontece no blog:'),
                m('.baloon-bg'),
                m('ul', ctrl.posts().map(function(post, index) {
                    if (index < 2) {
                        return m('li',
                            m('a.post', { href: post.URL,target: '_blank', onclick:open_external_link  }, [
                            m('h3', post.title),
                            m('p', [
                                m('em', [
                                    moment(post.date).format('DD/MM/YYYY'),
                                    ', por ',
                                    post.author.name
                                ]),
                                ' ',
                                post.excerpt.replace(/(<([^>]+)>)/ig, '').substring(0, 100),
                                '(...)'
                            ])
                        ]));
                    }
                }))
            ]),
            m('.links.pure-u-4-5.pure-u-sm-2-5', [
                topbar(),
                m('ul.content', [
                    m('li',
                        m('a[href=/contato]', {
                            config: m.route
                        }, 'Contato')
                    ),
                    m('li',
                        m('a[href=/politica-de-privacidade]', {
                            config: m.route
                        }, 'Política de privacidade')
                    ),
                    m('li',
                        m('a[href=/termos-de-uso]', {
                            config: m.route
                        }, 'Termos de uso')
                    )
                ])
            ]),
            m('.pure-u-1-5', [
                m('ul.social-links', [
                    m('li', [
                        m('a[href=https://www.facebook.com/pages/Plataforma-Brasil/762865640427489][target=_blank]',  {onclick:open_external_link }, [
                            m('i.icon-facebook'),
                            ' /PlataformaBrasil'
                        ])
                    ]),
                    /* m('li', [
                        m('a[href=https://play.google.com/store/apps/details?id=br.org.plataformabrasil.mobile][target=_blank]',  {onclick:open_external_link }, [
                            m('img.svg[src=img/icon/google-play.svg]'),
                             ' Baixe na Google Play'
                         ])
                    ]), */
                    /* m('li', [
                        m('a[href=#]', [
                            m('img.svg[src=/img/icon/app-store.svg][target=_blank]')
                         ])
                    ]) */
                ]),
                m('p', [
                    m('a[href=http://creativecommons.org/licenses/by-nc-sa/2.5/br/]', {onclick: open_external_link}, '(CC BY-NC-SA) Alguns direitos reservados')
                ])
            ])
        ])
    ]);
};

module.exports = footer;
