var m = require('mithril');
var badge_discussion = require('views/common/badge_discussion');
var theme_details = require('views/common/theme_details');
var form_new_comment = require('views/comments/form_new_comment');

var box_comments = function (ctrl, e) {
	var list_comments = [];
	var comments = ctrl.comments;
	var PAGE_SIZE = 10;
	var range = 0 + '-' + PAGE_SIZE;

	ctrl.updateListAfterRequest = function (data) {
		Array.prototype.push.apply(ctrl.comments(), data);
		//this.totalRecords.
	};

	if (comments() !== null) {
		var currentTotalComments = comments().length;

		if (currentTotalComments > 0) {
			var offset = currentTotalComments + PAGE_SIZE;
			range = currentTotalComments + '-' + offset;
		}

		comments().forEach(function(content, k) {
			list_comments.push(badge_discussion(content, 'comments', true, false, true, false));
		});
	} else {
		list_comments.push([m('.no-results.pure-u-4-5', [
			m('p', 'Não existem comentários para esta discussão ainda, seja o primeiro!')
		])]);
	}
    return [
		theme_details('compact', ctrl.featured_discussion().theme),
		m('.bd-box-post.container', badge_discussion(ctrl.featured_discussion(), 'comments', true, true, true, true)),
		m('.container.comments-list',[
			m('.pure-u-sm-22-24.pure-u-1', [
				m('.pure-u-sm-1-5'),
				m('.pure-u-sm-4-5.pure-u-1', [
					//form_new_comment(ctrl),
					list_comments
				])
			])
		]),
		(  (comments() !== null) && (comments().length < ctrl.total_records()) ?
			m('.center-content',
				m('a.pure-input-1.pure-button.btn-send-icon#btn-read-more[href=#]', 
					{ onclick: ctrl.get_more_comments.bind(this, ctrl, range) },
					'mais comentários'
				)
			)
		: '')
	];

};

module.exports = box_comments;