var m = require('mithril');
var baloon_bottom = require('views/common/baloon_bottom');

var configModel = require('models/config');
//var md2html = require('markdown2mithril');
function validateUrl(url) {
	var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
	var regex = new RegExp(expression);
	return url.match(regex);
}

function controller () {
	var ctrl = this;

	ctrl.page_title_relatoria_fase_3 = m.prop();
    ctrl.page_subtitle_relatoria_fase_3 = m.prop();
    ctrl.page_intro_relatoria_fase_3 = m.prop();
    ctrl.page_url_completo_relatoria_fase_3 = m.prop();
    ctrl.page_url_tema_1_relatoria_fase_3 = m.prop();
    ctrl.page_url_tema_2_relatoria_fase_3 = m.prop();
    ctrl.page_url_tema_3_relatoria_fase_3 = m.prop();
    ctrl.page_url_tema_4_relatoria_fase_3 = m.prop();
    ctrl.page_url_tema_5_relatoria_fase_3 = m.prop();

	configModel.get_site("page_title_relatoria_fase_3").then(function(data) { ctrl.page_title_relatoria_fase_3(data); });
	configModel.get_site("page_subtitle_relatoria_fase_3").then(function(data) { ctrl.page_subtitle_relatoria_fase_3(data); });
	configModel.get_site("page_intro_relatoria_fase_3").then(function(data) { ctrl.page_intro_relatoria_fase_3(data); });
	configModel.get_site("page_url_completo_relatoria_fase_3").then(function(data) { ctrl.page_url_completo_relatoria_fase_3(data); });
	configModel.get_site("page_url_tema_1_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_1_relatoria_fase_3(data); });
	configModel.get_site("page_url_tema_2_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_2_relatoria_fase_3(data); });
	configModel.get_site("page_url_tema_3_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_3_relatoria_fase_3(data); });
	configModel.get_site("page_url_tema_4_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_4_relatoria_fase_3(data); });
	configModel.get_site("page_url_tema_5_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_5_relatoria_fase_3(data); });

	var download = function(url, redirect) {
		if (validateUrl(url)) { window.open(url); m.route(redirect); }
		return false;
	};

	this.download_success_completo = function(url) {
		return download(url, '/download-relatorio-completo');
	};
	this.download_success_tema_1 = function(url) {
		return download(url, '/download-relatorio-tema-1');
	};
	this.download_success_tema_2 = function(url) {
		return download(url, '/download-relatorio-tema-2');
	};
	this.download_success_tema_3 = function(url) {
		return download(url, '/download-relatorio-tema-3');
	};
	this.download_success_tema_4 = function(url) {
		return download(url, '/download-relatorio-tema-4');
	};
	this.download_success_tema_5 = function(url) {
		return download(url, '/download-relatorio-tema-5');
	};
}

function view(ctrl) {
    return m('.statistics-page', [
        m('.hero-report', [
            m('.container', [
                m('.ilustration.pure-u-sm-1-3', [
                    m('img[src=/img/illustration/step3.svg]'),
                ]),
                m('.featured.pure-u-sm-2-3', [
                    m('h2', ctrl.page_title_relatoria_fase_3()),
                    baloon_bottom()
                ])
            ])
        ]),
        m('.container', [
            m('.pure-g', [
                m('.pure-u-20-24.content', [
                    m('h3', ctrl.page_subtitle_relatoria_fase_3()),

                    ctrl.page_intro_relatoria_fase_3(),

                    m('a.pure-button.full[href='+ctrl.page_url_completo_relatoria_fase_3()+'][target=_blank]',
					{onclick: ctrl.download_success_completo.bind(this, ctrl.page_url_completo_relatoria_fase_3())},
					[
                        m('i.icon-estatisticas'),
						'Relatório final do Ciclo 1 sobre Reforma Política do Século 21'
                    ]),
                    m('a.pure-button.full[href='+ctrl.page_url_tema_1_relatoria_fase_3()+'][target=_blank]',
					{onclick: ctrl.download_success_tema_1.bind(this, ctrl.page_url_tema_1_relatoria_fase_3())},
					[
                        m('i.icon-estatisticas'),
						'Relatório final Tema 1 : Decisões Políticas Transparentes'
                    ]),
                    m('a.pure-button.full[href='+ctrl.page_url_tema_2_relatoria_fase_3()+'][target=_blank]',
					{onclick: ctrl.download_success_tema_2.bind(this, ctrl.page_url_tema_2_relatoria_fase_3())},
					[
                        m('i.icon-estatisticas'),
						'Relatório final Tema 2 : Financiamento de Campanhas Eleitorais'
                    ]),
                    m('a.pure-button.full[href='+ctrl.page_url_tema_3_relatoria_fase_3()+'][target=_blank]',
					{onclick: ctrl.download_success_tema_3.bind(this, ctrl.page_url_tema_3_relatoria_fase_3())},
					[
                        m('i.icon-estatisticas'),
						'Relatório final Tema 3 : Ampliação dos Espaços de Consulta à Sociedade'
                    ]),
                    m('a.pure-button.full[href='+ctrl.page_url_tema_4_relatoria_fase_3()+'][target=_blank]',
					{onclick: ctrl.download_success_tema_4.bind(this, ctrl.page_url_tema_4_relatoria_fase_3())},
					[
                        m('i.icon-estatisticas'),
						'Relatório final Tema 4 : Fiscalização e Transparência das Doações para as Campanhas Eleitorais'
                    ]),
                    m('a.pure-button.full[href='+ctrl.page_url_tema_5_relatoria_fase_3()+'][target=_blank]',
					{onclick: ctrl.download_success_tema_5.bind(this, ctrl.page_url_tema_5_relatoria_fase_3())},
					[
                        m('i.icon-estatisticas'),
						'Relatório final Tema 5 : Participação Cidadã na Internet'
                    ])
                    // m('h4', 'DISCUTA NO BLOG'),
                    // m('ul.statistics-blog', [
                    //     m('li', [
                    //         m('i.icon-notificacao'),
                    //         m('a[href=]', 'QUANTIDADE DE PARTICIPANTES')
                    //     ]),
                    //     m('li', [
                    //         m('i.icon-notificacao'),
                    //         m('a[href=]', 'OS TEMAS MAIS DISCUTIDOS')
                    //     ]),
                    //     m('li', [
                    //         m('i.icon-notificacao'),
                    //         m('a[href=]', 'CONTRIBUÇÕES POR TEMA')
                    //     ])
                    // ])
                ])
            ])
        ])
    ]);
}

module.exports = {
    controller: controller,
    view: view
};
