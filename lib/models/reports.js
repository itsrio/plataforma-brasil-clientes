var m = require('mithril');
var c = require('config');


var reports = {

    getWinners: function (order, direction) {
        var url = c.getApiHost() + '/list-choices?orderDirection=score&orderBy=desc';
        return m.request({method:'GET',url: url});
    },

    getReports_phase1: function(order, direction) {
        if (order === undefined) { order = 'score'; }
        if (direction === undefined) { direction = 'desc'; }
        var url = c.getApiHost() + '/list-choices?orderDirection='+direction+'&orderBy='+order;
        return m.request({method:'GET',url: url});
    }
};

module.exports = reports;