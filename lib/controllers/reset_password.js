var m = require('mithril');

var userModel = require('models/user');
var c = require('config');
var Auth = require('models/auth');
var headerM = require('controllers/header');

var fb_button_login = require('views/common/fb_button_login');

var Login = module.exports = {
    controller: function() {
        //Senha alterada com sucesso!
        var vm = {};
        vm.mensagemValidacao = m.prop("");
        vm.save_password = function (e) {
            e.preventDefault();
            var new_pass = e.target.new_pass.value;
            var repeat_new_pass = e.target.repeat_new_pass.value;
            var reset_code = m.route.param("reset_code");
            if (new_pass === repeat_new_pass) {
                m.request({
                    method: 'POST',
                    url: c.getApiHost() + '/change_password',
                    data: {
                        reset_code: reset_code,
                        password: new_pass
                    },
                    unwrapSuccess: function(res) {
                        vm.mensagemValidacao(m(".alert.alert-success", {
                            config: vm.fadesIn
                        }, res.message));
                        e.target.new_pass.value = '';
                        e.target.repeat_new_pass.value = '';
                        setTimeout(function () {
                            m.route('/entrar');
                        }, 3000);
                    }
                }).then({}, function (res) {
                        vm.mensagemValidacao(m(".alert.alert-error", {
                            config: vm.fadesIn
                        }, res.message));
                        e.target.new_pass.value = '';
                        e.target.repeat_new_pass.value = '';
                });
            } else {
                vm.mensagemValidacao(m(".alert.alert-error", {
                    config: vm.fadesIn
                }, 'Senhas não são iguais, tente novamente, por favor.'));
                e.target.new_pass.value = '';
                e.target.repeat_new_pass.value = '';
            }
        };
        this.vm = vm;
    },
    view: function(ctrl) {
        return [
            m('.container', [
                m('.pure-g.login-signup-box', [
                    m('.pure-u-1', [
                        m('h2.section-title', 'digite a nova senha'),
                        m('.baloon-bg.yellow'),
                    ]),
                    m('.pure-'),
                    m('.pure-u-1.pure-u-sm-12-24.signup-box', [
                        //m('h3.subsection-title', ''),
                        m('form.pure-form', {
                                onsubmit: ctrl.vm.save_password.bind(ctrl)
                            },
                            ctrl.vm.mensagemValidacao(),
                            m('input[required][type=password][name=new_pass][placeholder=Nova Senha]'),
                            m('input[required][type=password][name=repeat_new_pass][placeholder=Repetir Senha]'),
                            m('button.pure-button', {
                                type: 'submit'
                            }, 'enviar')
                        ),
                    ])
                ])
            ])
        ];
    }
};

