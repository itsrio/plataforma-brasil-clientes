var m = require('mithril');
var choose_tag = function (ctrl) {
    return [
            (ctrl.vm.popupTagOpened ?
                m('.popup.pure-g', [
                    m('.popup-box.' + ctrl.vm.sectorPopup(), [
                        m('.pure-hidden-sm.pure-u-sm-4-24'),
                        m('.pure-u-1.pure-u-sm-8-24', [
                            m('img.illustration[src=/img/illustration/' + ctrl.vm.sectorPopup() + '.svg]'),
                            m('.tag-box', [
                                m('img.pure-hidden-sm.svg.icon[src=/img/icon/' + ctrl.vm.sectorPopup() + '.svg]'),
                                m('.text', '.' + ctrl.vm.sectorPopup())
                            ])
                        ]),
                        m('.pure-u-1.pure-u-sm-8-24', [
                            m('p', ctrl.vm.sectorDescriptionPopup()),
                            m('a.pure-button.cancelar', {
                                onclick: ctrl.vm.fecharTagPopup
                            }, 'cancelar'),
                            m('a.pure-button[id=' + ctrl.vm.sectorPopup() + ']', {
                                onclick: m.withAttr('id', ctrl.vm.sectorClick)
                            }, 'escolher')
                        ]),
                        m('.pure-hidden-sm.pure-u-sm-4-24'),
                    ]),
                    m('.triangle')
                ]) : ''),
                m('.pure-g', [
            m('.pure-u-1.pure-u-sm-4-24.sector.gov[id=gov]', {
                onclick: ctrl.vm.openPopupClick.bind(ctrl, this)
            }, [
                m('img.pure-hidden-sm.illustra.pure-img[id=gov][src=/img/illustration/gov.svg]'),
                m('.button[id=gov]', [
                    m('img.svg[id=gov][src=/img/icon/gov.svg]'),
                    m('span', '.gov')
                ])
            ]),
            m('.pure-u-1.pure-u-sm-4-24.sector.edu[id=edu]', {
                onclick: ctrl.vm.openPopupClick.bind(ctrl, this)
            }, [
                 m('img.pure-hidden-sm.illustra.pure-img[id=edu][src=/img/illustration/edu.svg]'),
                m('.button[id=edu]', [
                    m('img.svg[id=edu] [src=/img/icon/edu.svg]'),
                    m('span', '.edu')
                ])
            ]),
            m('.pure-u-1.pure-u-sm-4-24.sector.ong[id=ong]', {
                onclick: ctrl.vm.openPopupClick.bind(ctrl, this)
            }, [
                m('img.pure-hidden-sm.illustra.pure-img[id=ong][src=img/illustration/ong.svg]'),
                m('.button[id=ong]', [
                    m('img.svg[id=ong][src=/img/icon/ong.svg]'),
                     m('span', '.ong')
                 ])
            ]),
            m('.pure-u-1.pure-u-sm-4-24.sector.com[id=com]', {
                onclick: ctrl.vm.openPopupClick.bind(ctrl, this)
            }, [
                 m('img.pure-hidden-sm.illustra.pure-img[id=com][src=/img/illustration/com.svg]'),
                m('.button[id=com]', [
                    m('img.svg[id=com][src=/img/icon/com.svg]'),
                     m('span', '.com')
                 ])
            ]),
            m('.pure-u-1.pure-u-sm-4-24.sector.br[id=br]', {
                onclick: ctrl.vm.openPopupClick.bind(ctrl, this)
            }, [
                 m('img.pure-hidden-sm.illustra.pure-img[id=br][src=/img/illustration/br.svg]'),
                m('.button[id=br]', [
                    m('img.svg[id=br][src=/img/icon/br.svg]'),
                     m('span', '.br')
                 ])
            ])
                ])
    ];
};

module.exports = choose_tag;
