var m = require('mithril');
var c = require('config');
var userModel = require('models/user');

var discussionModel = require('models/discussion');

var my_answers = require('views/profile/my_answers');

var my_discussion = module.exports = {
    controller: function() {
        var ctrl = this;
		ctrl.my_answers = m.prop([]);
        ctrl.current_user = m.prop(userModel.isAuthenticate(localStorage.user))
		.then(function (user) {
			discussionModel.get_mine(user.id).then(function (data) {
				ctrl.my_answers(data);
			});
		});

    },

    view: function(ctrl) {
        return [
            m('.my-discussions.container',
                my_answers(ctrl)
            )
        ];
    }
};
