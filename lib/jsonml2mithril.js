var type = require('type-of-is');

module.exports = function parse(input) {
	if (type(input, String)) {
		return input;
	} else if (type(input[0], Array)) {
		return input.map(parse);
	}

	return input.reduce(function (node, item, index) {
		switch (type(item)) {
			case String:
				if (index === 0) {
					node.tag = item;
				} else {
					node.children.push(item);
				}
				break;
			case Array:
				node.children.push(parse(item));
				break;
			case Object:
				node.attrs = item;
		}

		return node;
	}, {
		attrs: {},
		children: []
	});
};
