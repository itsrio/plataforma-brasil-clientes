var m = require('mithril');

var handy_link = function (ctrl, content) {
//	title, link, image_link) {
	return [
		m('.pure-u-sm-1-2.pure-u-1.box-link', [
			m('.content', [
				m('img.pure-img[src='+content.image+']'),
				m('.title', [
					m('h3', content.title),
					m('a[href='+content.url+'][target=_blank]', content.url)
				])
			]),
			m('a.more[href='+content.url+']', 'leia mais')
		])
	];
};

module.exports = handy_link;
