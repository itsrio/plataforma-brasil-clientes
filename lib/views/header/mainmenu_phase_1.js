
var m = require('mithril');

var mainmenu = function () {
	return [
			m('ul.main-menu', [
				m('li', [
					m('a.menu-item[href=/fase-1-votar]', {config: m.route},[
						m('i.icon-priorizar'),
						m('span.small', 'participar da'),
						m('span.big', 'Priorização')
					])
				]),
				m('li', [
					m('a.menu-item[href=/fase-1-resultados]', {config: m.route},[
						m('i.icon-resultados'),
						m('span.small', 'acompanhar'),
						m('span.big', 'resultados')
					])
				])
			])
	];
};
module.exports = mainmenu;
