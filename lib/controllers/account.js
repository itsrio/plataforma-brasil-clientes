var m = require('mithril');
var userModel = require('../models/user');
var badge_user = require('views/common/badge_user');

var account = {};

account.controller = function() {
    var ctrl = this;
    this.vm = account.vm;

    userModel.isAuthenticate()
        .then(function(data) {
            account.vm.user(data);
        });

    ctrl.current_user = m.prop(userModel.isAuthenticate(localStorage.user));
};

account.vm = {
    user: m.prop({})
};

account.view = function(ctrl) {

    return m('.meu-perfil', [
        m('.content.container', [
            m('.pure-g', [
                m('.pure-u-1.pure-u-sm-8-24', [
                    badge_user(ctrl.current_user(), 200)
                ]),
                m('.pure-u-1.pure-u-sm-6-24.profile-menu', [
                    m('h2', m('a[href=/fase-1-meus-resultados]',{config: m.route}, '>> meus resultados')),
                    m('h2', m('a[href=/meu-perfil-editar]',{config: m.route}, '>> editar perfil')),
                    m('h2', m('a[href=/sair]', {config: m.route}, '>> sair'))
                ]),
                m('.pure-u-1.pure-u-sm-10-24', [
                    m('.profile-details',
                        m('.pure-u-1-5', m('.separator')),
                        m('.pure-u-4-5', [
                        m('h1', ctrl.vm.user().nome),
                        m('h3', ctrl.vm.user().ocupacao),
                        m('h3', ctrl.vm.user().cidade + ' - ' + ctrl.vm.user().estado),
                        m('h3', ctrl.vm.user().email),
                        m('p', ctrl.vm.user().descricao)
                        ])
                    )
                ]),
            ]),

            m('.pure-g', [
                m('.pure-u-1.config', [
                    m('.container', [
                        m('h2', [
                            m('img[src=img/icon/perfil-configuracao.svg]'),
                            'configurações'
                        ])
                    ]),
                    m('.pure-form', [
                        m('label.agree[for=novidades]', [
                            m('input[type=checkbox][name=privacidade][checked][id=novidades]'),
                            'QUERO RECEBER NOVIDADES DA PLATAFORMA EM MEU EMAIL'
                        ]),
                        m('label.agree[for=notificacoes]', [
                            m('input[type=checkbox][name=privacidade][checked][id=notificacoes]'),
                            'QUERO RECEBER NOTIFICAÇÕES DA PLATAFORMA EM MEU EMAIL'
                        ]),
                    ]),
                ]),
                m('.pure-u-1.config', [
                    m('.container', [
                        m('h2', [
                            m('img[src=img/icon/perfil-privacidade.svg]'),
                            'privacidade'
                        ])
                    ]),
                    m('.pure-form', [
                        m('label.agree[for=privacidade][for=esconder_email]', [
                            m('input[type=checkbox][name=privacidade][checked][id=esconder_email]'),
                            'ESCONDER MEU EMAIL'
                        ]),
                        m('label.agree[for=privacidade][for=esconder_infos]', [
                            m('input[type=checkbox][name=privacidade][checked][id=esconder_infos]'),
                            'ESCONDER MINHAS INFORMAÇÕES PESSOAIS'
                        ]),


                    ])
                ]),
            ]),
            m('br')
        ])
    ]);
};

module.exports = account;
