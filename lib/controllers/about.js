var m = require('mithril');
var creditos_foto = require('views/common/creditos_foto');
var open_external_link = require('views/common/open_external_link');
var configModel = require('models/config');
var md2html = require('markdown2mithril');
var about = {};

about.controller = function() {
    var desc = m.prop('');
    configModel.get_site('page_sobre').then(function(data) {
        desc(data);  
    });

    return {
        content : function () {
            return md2html(desc());
        }
    };
};

about.view = function(ctrl) {
    return m('.page', [
        m('.hero', [
            m('.title.pure-u-4-5.pure-u-sm-3-5', [
                m('h1', 'Quem somos')
            ]),
            creditos_foto()
        ]),
        m('.container', [
            m('.pure-g', [
                m('.pure-u-1.content', [
                    ctrl.content(),
                    m('form[action=https://pagseguro.uol.com.br/checkout/v2/donation.html][method=post]', [
                        m('input[type=hidden][name=currency][value=BRL]'),
                        m('input[type=hidden][name=receiverEmail][value=max.holender@itsrio.org]'),
                        m('input[type=image][src=https://p.simg.uol.com.br/out/pagseguro/i/botoes/doacoes/120x53-doar.gif][name=submit][alt=Pague com PagSeguro - é rápido, grátis e seguro!]')
                    ])
                ])
            ])
        ])
    ]);
};

module.exports = about;
