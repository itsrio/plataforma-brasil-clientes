var m = require('mithril');
var feature_box= require('views/common/feature_box');
var creditos_foto = require('views/common/creditos_foto');
var steps = require('views/home/phase_1_steps');

// Um jeito construtivo de fazer políticas públicas
module.exports = function () {
    return [
        m('.hero.home', [
            m('.container', [
                m('.pure-g', feature_box(
                    m('img.svg.logo-square[src=img/logo.svg]'),
                    m('h1', 'Um jeito construtivo de fazer políticas públicas')
                ))
            ]),
            creditos_foto()
        ]),
        m('.pure-g',
            m('.pure-u-1',
                m('.entenda-box', [
                  m('span', 'entenda'),
                  m('i.icon-triangle-down')
                ])
             )
         ),
        m('.steps', [
            m('.container', [
                steps()
            ])
        ])
    ];
};
