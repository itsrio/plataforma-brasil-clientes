var m = require('mithril');
var invite_friend = function (ctrl) {
    return [
    m('.pure-g',[
        m('.pure-u-sm-4-24.pure-hidden-sm'),
        m('.convidar-box.pure-u-1.pure-u-sm-16-24', [
            m('.pure-u-3-24', [
                m('.separator')
            ]),
            m('.pure-u-18-24.pure-form', [
                m('h2', 'peça ajuda de um amigo'),
                ctrl.statusEnviarAjuda(),
                m('input[type=email][name=email-amigo][placeholder=EMAIL DO AMIGO]', {
                    onchange: m.withAttr('value', ctrl.emailParaAmigo),
                    value: ctrl.emailParaAmigo()
                }),
                m('textarea[name=mensagem-amigo][rows=9][placeholder=MENSAGEM PARA AMIGO]', {
                    onchange: m.withAttr('value', ctrl.mensagemParaAmigo),
                    value: ctrl.mensagemParaAmigo()
                }),
                m('submit.pure-button', {
                    onclick: ctrl.enviarAjudaAmigo.bind(ctrl, this)
                }, 'Enviar')
            ]),
            m('.convidar-box.pure-u-3-24', [
                m('a[href=#]', {
                    onclick: ctrl.fecharAjudaAmigo.bind(ctrl, this)
                }, m('img[src=img/icon/fechar.svg]'))
            ])
        ])
    ])
    ];
};
module.exports = invite_friend;
