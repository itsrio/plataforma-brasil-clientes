/*jshint expr: true */
/*global describe, it */

var should = require('should');
var header = require('../../lib/modules/header');

describe('header', function () {

  describe('controller', function () {

    it('should be a function', function () {
      header.controller.should.be.a.function;
    });
/*
    it('should return an object', function () {
      header.controller().should.be.a.object;
    });
*/
  });

  describe('view', function () {

    it('should be a function', function () {
      header.view.should.be.a.function;
    });
/*
    it('should return an object', function () {
      header.view().should.be.a.object;
    });
*/
  });

});
