var m = require('mithril');
var c = require('config');
var _ = require('underscore');
var header_phase = require('views/header/header_phases');
var topbar = require('views/header/topbar');
var mainmenu = require('views/header/mainmenu');
var login = require('views/header/login');
var loggedin = require('views/header/loggedin');
var header = {};

header.view = function(ctrl) {
    var u = ctrl.vm.user();
    if (_(u).size() < 1) {
        ctrl.actions = login(ctrl);
    } else {
        ctrl.actions = loggedin(ctrl);
    }

    return header.common(ctrl);
};

header.top = function(ctrl) {
    return m('.topbar.pure-hidden-sm', [
            m('.container.pure-g', [
                m('.pure-u-1', topbar())
            ])
        ]);
};

header.common = function(ctrl) {
    return [
        m('#header', [
            header.top(),
            m('.pure-g.container.menu', [
                header_phase(ctrl),
                m('.pure-hidden-sm.pure-u-sm-12-24', mainmenu(ctrl)),
                m('.pure-hidden-sm.pure-u-sm-8-24',[
                    ctrl.actions
                ])
            ]),
        ]),
        m('#mobile-menu.mobile-menu.pure-u-1.pure-visible-only-sm', [
            ctrl.actions,
            mainmenu(ctrl),
            topbar()
        ])
    ];
};

module.exports = header;
