var m = require('mithril');
var headerM = require('controllers/header');
var Auth = require('models/auth');
var Cookies = require('cookies-js');
var logout = {};

logout.controller = function() {
    this.vm = logout.vm;
    this.vm.removeSession();
};

logout.vm = {
    removeSession: function() {
        localStorage.clear();
        var cookie_defaults = { domain : '.' + document.domain, expires: Infinity };
        Cookies.expire('user_email',cookie_defaults);
        Cookies.expire('user_photourl',cookie_defaults);
        Cookies.expire('user_setor',cookie_defaults);
        Cookies.expire('user_nome',cookie_defaults);
        Cookies.expire('logged_in',cookie_defaults);
        
        m.module(document.getElementById('header'), headerM);

        // quick and dirty solution for weird state on header after logout
        // just pretend you didn't see this =P
        window.location.href = '/';

        // m.route('/');
    }
};

logout.view = function(ctrl) {
    return m('div');
};

module.exports = logout;
