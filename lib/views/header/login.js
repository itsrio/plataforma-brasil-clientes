var m = require('mithril');
var c = require('config');
var fb_button_login = require('views/common/fb_button_login');

var login = function(ctrl) {
    return m('a[href=https://www.mudamos.org]', m('img[src=/img/aviso-mudamos.svg][width=246][style=margin: -7px 25px 0 0;float: right;]'));
    return m('ul.login-menu', [
        m('li', fb_button_login()),
        m('li', [
            m('a.menu-item[href=/entrar]', {config: m.route}, [
                m('i.icon-cadastrar-email'),
                m('span.small', 'login ou cadastro'),
                m('span.big', 'email')
            ])
        ])
    ]);
};

module.exports = login;
