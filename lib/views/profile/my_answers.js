var m = require('mithril');
var top_header = require('views/profile/top_header');
var divisor = require('views/profile/divisor');
var badge_discussion = require('views/common/badge_discussion');

var my_answers = function (ctrl) {
	var list_discussions = [];
	if (ctrl.my_answers()) {
		ctrl.my_answers().forEach(function(discussion, k) {
			list_discussions.push(badge_discussion(discussion, 'my_answers', false, true, true, true));
		});
	} else {
		list_discussions.push(
			m('.notification-list.profile-info-notification',
				m('.notification-post','Sem Dados')
			)
		);
	}
    return [
//		top_header(ctrl,'digitalnucleo','Eu','ong'),
		m('.pure-u-1.pure-u-sm-2-5', divisor('icon-traco-lapis','','Minhas Respostas' )),
		m('.pure-u-1.pure-u-sm-3-5', [
			m('.container', list_discussions)
		])
	];
};

module.exports = my_answers;

//TO DO: Corrigir msg se não houver dados