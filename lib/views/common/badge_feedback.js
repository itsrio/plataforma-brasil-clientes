var m = require('mithril');
module.exports = function (message) {
	this.fadesOut = function(element, isInitialized, context) {
		if (!isInitialized) {
			element.style.opacity = 1;
			Velocity(element, {
				opacity: 0
			}, {duration: 5000});
		}
	};
	if (message() !== undefined) {
		return message().map(function(data) {
			return m('p.alert-' + data.type, data.text);
		});
	}
};
