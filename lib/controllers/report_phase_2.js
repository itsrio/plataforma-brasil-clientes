var m = require('mithril');
var baloon_bottom = require('views/common/baloon_bottom');
var themeModel = require('models/theme');
var discussionModel = require('models/discussion');
var badge_discussion = require('views/common/badge_discussion');
var configModel = require('models/config');
//var md2html = require('markdown2mithril');

var ENTER_KEY = 13;

function controller () {
    var ctrl = this;
    var theme_id = m.route.param("theme_id");
    ctrl.theme_id = m.prop(theme_id);
    ctrl.themes = m.prop([]);
    ctrl.theme_active = m.prop({}); // get all details from theme
    ctrl.list_discussions = m.prop([]);
    ctrl.author = m.prop(m.route.param('autor'));
    ctrl.category = m.prop(m.route.param('categoria'));
    ctrl.order = m.prop(m.route.param('ordenar'));
    if (ctrl.author() === undefined) {
        ctrl.author('');
    }
    if (ctrl.order() === undefined) {
        ctrl.order('mais-quentes');
    }
    ctrl.orderOptions = m.prop([
        { name : 'Mais Quentes', icon : 'icon-quente', slug: 'mais-quentes'},
        { name : 'Mais Frias', icon : 'icon-quente', slug: 'mais-frias'},
        { name : 'Mais Novas', icon : 'icon-estatisticas', slug: 'mais-novas'},
        { name : 'Mais Antigas', icon : 'icon-estatisticas', slug: 'mais-antigas'},
    ]);
    ctrl.total_records = m.prop(0);
    ctrl.get_more_discussions = discussionModel.get_more_discussions;

    this._getFilters = function() {
        var url = '?theme_id='+ctrl.theme_active().id;
        if (ctrl.category() !== undefined) {
            url += '&categoria='+ctrl.category();
        }
        if (ctrl.author() !== undefined) {
            url += '&autor='+ctrl.author();
        }
        if (ctrl.order() !== undefined) {
            url += '&ordenar='+ctrl.order();
        }
        return url;
    };

    this.setAuthor = function(ctrl, e) {
        m.route('/comentarios-fase-2/'+ctrl._getFilters());
        return false;
    };

    this.setOrder = function(ctrl, e) {
        ctrl.order(e.currentTarget.getAttribute('data-slug'));
        m.route('/comentarios-fase-2/'+ctrl._getFilters());
        return false;
    };

	this.setCategory = function (ctrl, e) {
        var slug = e.currentTarget.getAttribute('data-slug');

        // if clicked on same category as before, disable it
        if (slug == ctrl.category()) slug = undefined;

        ctrl.category(slug);
        m.route('/comentarios-fase-2/' + ctrl._getFilters());
        return false;
    };

    ctrl.page_title_relatoria_fase_2 = m.prop();
    ctrl.page_intro_relatoria_fase_2 = m.prop();

    configModel.get_site('page_title_relatoria_fase_2').then(function(data) { ctrl.page_title_relatoria_fase_2(data); });
    configModel.get_site('page_intro_relatoria_fase_2').then(function(data) { ctrl.page_intro_relatoria_fase_2(data); });

    ctrl.theme_title_relatorio_fase_2 = m.prop();
    ctrl.theme_intro_relatorio_fase_2 = m.prop();

    themeModel.getAll().then(function(data) {
        ctrl.themes(data);
        if (theme_id === undefined) {
            theme_id = data[0].id;
        }
        // get all details from theme
        themeModel.get(theme_id).then(ctrl.theme_active).then(function() {
            var pos = ctrl.theme_active().position;
            configModel.get_site('page_theme_0'+pos+'_title_relatoria_fase_2').then(
                ctrl.theme_title_relatorio_fase_2);
            configModel.get_site('page_theme_0'+pos+'_intro_relatoria_fase_2').then(
                ctrl.theme_intro_relatorio_fase_2);
        });
        discussionModel.get_all(theme_id, ctrl.order(), ctrl.category(), ctrl.author()).then(function (data) {
            if (data !== null) {
                ctrl.list_discussions(data);
                ctrl.total_records(discussionModel.total_records());
            }
        });
    });

}

function view(ctrl) {
    var PAGE_SIZE = 10;
    var range = 0 + '-' + PAGE_SIZE;

    ctrl.updateListAfterRequest = function (data) {
        Array.prototype.push.apply(ctrl.list_discussions(), data);
        //this.totalRecords.
    };
    var view_discussions = m.prop([]);

    if (ctrl.list_discussions() !== null && ctrl.list_discussions().length > 0) {
        var currentTotalDiscussions = ctrl.list_discussions().length;

        if (currentTotalDiscussions > 0) {
            //var offset = currentTotalDiscussions + PAGE_SIZE;
            range = currentTotalDiscussions + '-' + PAGE_SIZE;
        }

        ctrl.list_discussions().forEach(function(content, k) {
            view_discussions().push(badge_discussion(content, 'Discussions', true, true, true, false));
        });
    } else {
        var msg = (ctrl.list_discussions() === null) ?
            'Não existem discussões para este tema ainda, comece uma!' :
            'Nenhuma discussão foi encontrada!';
        view_discussions().push([
            m('.no-results.pure-u-4-5', [
                m('p', msg)
            ])
        ]);
    }

    return m('.report-phase-2', [
        m('.hero-report', [
            m('.container', [
                m('.ilustration.pure-u-sm-1-3', [
                    m('img[src=/img/illustration/step2.svg]'),
                ]),
                m('.featured.pure-u-sm-2-3', [
                    m('h2', [
                        'VEJA COMO FOI O DEBATE:',
                        m('br'),
                        m('span', ctrl.theme_active().name)
                    ]),
                    m('ul.theme-menu', ctrl.themes().map(function(theme) {
                        var className = (ctrl.theme_active().id == theme.id ? '.active' : '');
                        return m('li' + className, m('a[href=/comentarios-fase-2/?theme_id='+theme.id+']', {config: m.route}, ("0" + theme.position).substr(-2,2)));
                    }))
                ]),
            ]),
        ]),
        m('.container', [
            m('h3', ctrl.page_title_relatoria_fase_2()),
            ctrl.page_intro_relatoria_fase_2(),

            m('h3', ctrl.theme_title_relatorio_fase_2()),
            ctrl.theme_intro_relatorio_fase_2(),

            m('.pure-g.filter-report', [
                m('.pure-u-1', [
                    m('p.label', [
                        'FILTRE POR CATEGORIAS: ',
                        m('.hint--top', {'data-hint':'Filtro para visualizar as respostas por palavras-chave'}, m('i.icon-ajuda'))
                    ]),
                    m('ul.list-categories', ctrl.theme_active().categories.map(function(category) {
                        var className = (ctrl.category() == category.slug ? '.active' : '');
                        return m('li'+className, m('a[href=#][data-slug='+category.slug+'][data-id='+category.id+']',
                                        { onclick: ctrl.setCategory.bind(this, ctrl) },
                                        category.name.toUpperCase()
                        ));
                    })),
                ]),
                m('.pure-u-1.pure-u-sm-4-5', [
                    m('p.label', [
                        'ORDENAÇÃO: ',
                        m('.hint--top', {'data-hint':'Escolha a forma que você quer visualizar as respostas dos participantes'}, m('i.icon-ajuda'))
                    ]),
                    m('ul.list-order', ctrl.orderOptions().map(function(option) {
                        var className = (ctrl.order() == option.slug ? '.active' : '');
                        return m('li'+className, m('a[href=#][data-slug='+option.slug+']', { onclick: ctrl.setOrder.bind(this, ctrl) }, [option.name.toUpperCase(), m('i.' + option.icon)]));
                    }))
                ]),
                m('.pure-u-1.pure-u-sm-1-5', [
                    m('p.label', [
                        'BUSCA POR AUTOR: ',
                        m('.hint--top', {'data-hint':"Busque aqui as respostas \n por nome de participante"}, m('i.icon-ajuda'))
                    ]),
                    m('.search', [
                        m('input[type=text]', {
                            onchange: m.withAttr("value", ctrl.author),
                            onkeydown: function (evt) {
                                m.withAttr("value", ctrl.author)(evt);
                                if (evt.keyCode == ENTER_KEY) {
                                    ctrl.setAuthor(ctrl);
                                }
                            },
                            value: ctrl.author()
                        }),
                        m('a.pure-button', { onclick: ctrl.setAuthor.bind(this, ctrl) },  m('i.icon-busca'))
                    ])
                ])
            ]),
            view_discussions(),
            (  (ctrl.list_discussions() !== null) && (ctrl.list_discussions().length < ctrl.total_records()) ?
                m('.center-content',
                    m('a.pure-input-1.pure-button.btn-send-icon#btn-read-more[href=#]',
                        { onclick: ctrl.get_more_discussions.bind(this, ctrl, range) },
                        'carregar mais'
                    )
                )
            : '')
        ])
    ]);
}

module.exports = {
    controller: controller,
    view: view
};


