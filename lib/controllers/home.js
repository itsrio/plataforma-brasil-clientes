var m = require('mithril');
var _ = require('underscore');

var userModel = require('models/user');
var phaseModel = require('models/phase');
var themeModel = require('models/theme');
var discussionModel = require('models/discussion');
var configModel = require('models/config');

var phase_1 = require('views/home/phase_1');
var phase_2 = require('views/home/phase_2');
var phase_3 = require('views/home/phase_3');

var loginModule = require('controllers/login');

var home = {};

home.controller = function() {
    this.user = m.prop("");
    var ctrl = this;

    ctrl.titulo = m.prop('');
    ctrl.bg = m.prop('');
    ctrl.fase_1_duracao = m.prop('');
    ctrl.fase_1_nome = m.prop('');
    ctrl.fase_1_descricao = m.prop('');
    ctrl.fase_2_duracao = m.prop('');
    ctrl.fase_2_nome = m.prop('');
    ctrl.fase_2_descricao = m.prop('');
    ctrl.fase_3_duracao = m.prop('');
    ctrl.fase_3_nome = m.prop('');
    ctrl.fase_3_descricao = m.prop('');

    configModel.get_site('home_titulo').then(function(data){ ctrl.titulo(data); });
    configModel.get_site('home_bg').then(function(data){ ctrl.bg(data); });

    configModel.get_site('fase_1_nome').then(function(data) { ctrl.fase_1_nome(data); });
    configModel.get_site('fase_1_duracao').then(function(data) { ctrl.fase_1_duracao(data); });
    configModel.get_site('fase_1_descricao').then(function(data) { ctrl.fase_1_descricao(data); });

    configModel.get_site('fase_2_nome').then(function(data) { ctrl.fase_2_nome(data); });
    configModel.get_site('fase_2_duracao').then(function(data) { ctrl.fase_2_duracao(data); });
    configModel.get_site('fase_2_descricao').then(function(data) { ctrl.fase_2_descricao(data); });

    configModel.get_site('fase_3_nome').then(function(data) { ctrl.fase_3_nome(data); });
    configModel.get_site('fase_3_duracao').then(function(data) { ctrl.fase_3_duracao(data); });
    configModel.get_site('fase_3_descricao').then(function(data) { ctrl.fase_3_descricao(data); });

    // TODO: read from config witch phase is active
	if (phaseModel.getCurrentActive() == 1) {
		ctrl.current_phase = phase_1;
	} else if (phaseModel.getCurrentActive() == 2) {
		ctrl.current_phase = phase_2;
		themeModel.getAll().then(function (data) {
			ctrl.themes = m.prop(data);
		});

		this.login_partial = new loginModule.controller();

		ctrl.discussions = m.prop([]);
		discussionModel.get_all().then(function (data) {
			if (data !== null) {
				ctrl.discussions(data);
			}
		});
	} else if (phaseModel.getCurrentActive() == 3) {

        ctrl.page_theme_01_title_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_01_intro_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_01_link_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_02_title_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_02_intro_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_02_link_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_03_title_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_03_intro_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_03_link_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_04_title_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_04_intro_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_04_link_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_05_title_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_05_intro_relatoria_fase_2 = m.prop('');
        ctrl.page_theme_05_link_relatoria_fase_2 = m.prop('');

        ctrl.page_url_tema_1_relatoria_fase_3 = m.prop();
        ctrl.page_url_tema_2_relatoria_fase_3 = m.prop();
        ctrl.page_url_tema_3_relatoria_fase_3 = m.prop();
        ctrl.page_url_tema_4_relatoria_fase_3 = m.prop();
        ctrl.page_url_tema_5_relatoria_fase_3 = m.prop();

        configModel.get_site('page_theme_01_title_relatoria_fase_2').then(function(data) { ctrl.page_theme_01_title_relatoria_fase_2(data); });
        configModel.get_site('page_theme_01_intro_relatoria_fase_2').then(function(data) { ctrl.page_theme_01_intro_relatoria_fase_2(data); });
        configModel.get_site('page_theme_01_link_relatoria_fase_2').then(function(data) { ctrl.page_theme_01_link_relatoria_fase_2(data); });
        configModel.get_site('page_theme_02_title_relatoria_fase_2').then(function(data) { ctrl.page_theme_02_title_relatoria_fase_2(data); });
        configModel.get_site('page_theme_02_intro_relatoria_fase_2').then(function(data) { ctrl.page_theme_02_intro_relatoria_fase_2(data); });
        configModel.get_site('page_theme_02_link_relatoria_fase_2').then(function(data) { ctrl.page_theme_02_link_relatoria_fase_2(data); });
        configModel.get_site('page_theme_03_title_relatoria_fase_2').then(function(data) { ctrl.page_theme_03_title_relatoria_fase_2(data); });
        configModel.get_site('page_theme_03_intro_relatoria_fase_2').then(function(data) { ctrl.page_theme_03_intro_relatoria_fase_2(data); });
        configModel.get_site('page_theme_03_link_relatoria_fase_2').then(function(data) { ctrl.page_theme_03_link_relatoria_fase_2(data); });
        configModel.get_site('page_theme_04_title_relatoria_fase_2').then(function(data) { ctrl.page_theme_04_title_relatoria_fase_2(data); });
        configModel.get_site('page_theme_04_intro_relatoria_fase_2').then(function(data) { ctrl.page_theme_04_intro_relatoria_fase_2(data); });
        configModel.get_site('page_theme_04_link_relatoria_fase_2').then(function(data) { ctrl.page_theme_04_link_relatoria_fase_2(data); });
        configModel.get_site('page_theme_05_title_relatoria_fase_2').then(function(data) { ctrl.page_theme_05_title_relatoria_fase_2(data); });
        configModel.get_site('page_theme_05_intro_relatoria_fase_2').then(function(data) { ctrl.page_theme_05_intro_relatoria_fase_2(data); });
        configModel.get_site('page_theme_05_link_relatoria_fase_2').then(function(data) { ctrl.page_theme_05_link_relatoria_fase_2(data); });

        configModel.get_site("page_url_tema_1_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_1_relatoria_fase_3(data); });
        configModel.get_site("page_url_tema_2_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_2_relatoria_fase_3(data); });
        configModel.get_site("page_url_tema_3_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_3_relatoria_fase_3(data); });
        configModel.get_site("page_url_tema_4_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_4_relatoria_fase_3(data); });
        configModel.get_site("page_url_tema_5_relatoria_fase_3").then(function(data) { ctrl.page_url_tema_5_relatoria_fase_3(data); });

		ctrl.current_phase = phase_3;
		themeModel.getAll().then(function (data) {
			ctrl.themes = m.prop(data);
		});
	}

    userModel.isAuthenticate("")
        .then(function(data) {
            ctrl.user(data);
        });
};

home.view = function(ctrl) {
	return [
		ctrl.current_phase(ctrl)
	];
};

module.exports = home;
