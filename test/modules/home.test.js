/*jshint expr: true */
/*global describe, it */

var should = require('should');
var home = require('../../lib/modules/home');

describe('home', function () {

  describe('controller', function () {

    it('should be a function', function () {
      home.controller.should.be.a.function;
    });

    it('should return an object', function () {
      home.controller().should.be.a.object;
    });

  });

  describe('view', function () {

    it('should be a function', function () {
      home.view.should.be.a.function;
    });

    // it('should return an object', function () {
    //   home.view().should.be.a.object;
    // });

  });

});
