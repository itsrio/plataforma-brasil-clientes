var jsonml2m  = require('./jsonml2mithril');
var md2jsonml = require('./markdown2jsonml');

module.exports = function (markdown) {
  return jsonml2m(md2jsonml(markdown));
};
