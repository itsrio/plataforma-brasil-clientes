var m = require('mithril');
var userModel = require('models/user');
var phaseModel = require('models/phase');
var c = require('config');
var Auth = require('models/auth');
var headerM = require('controllers/header');
var Cookies = require('cookies-js');
var fb_button_login = require('views/common/fb_button_login');

var Login = module.exports = {
    controller: function() {
        var vm = {};
        vm.cpf = m.prop("");
        vm.user = m.prop("");

        vm.visibleForgotPassword = false;
        vm.toggleForgotPassword = function() {
            if (vm.visibleForgotPassword) {
                vm.visibleForgotPassword = false;
            } else {
                vm.visibleForgotPassword = true;
            }
        };

        this.resetPassword = function (e) {
            e.preventDefault();
            var email = e.target.reset_email.value;
            m.request({
                method: 'GET',
                url: c.getApiHost() + '/reset_password/' + email,
                unwrapSuccess: function(res) {
                    vm.mensagemResetValidacao(m(".alert.alert-success", {
                        config: vm.fadesIn
                    }, res.message));
                    e.target.reset_email.value = '';
                }
            });
        };

        vm.mensagemResetValidacao = m.prop("");
        vm.mensagemValidacao = m.prop("");
        vm.mensagemLoginValidacao = m.prop("");

        vm.cpfMask = function(value) {
            value = value.replace(/\D/g, '');
            value = value.substr(0, 11);
            vm.cpf(value);
        };

        vm.validateCPF = function(value) {
            vm.cpfMask(value);
            var clean_value = vm.cpf();
            if ((clean_value !== "") && (!vm.checkCPF(clean_value))) {
                vm.mensagemValidacao(m(".alert.alert-error", {
                    config: vm.fadesIn
                }, "CPF Inválido."));
            } else {
                vm.mensagemValidacao("");
            }
        };

        vm.fadesIn = function(element, isInitialized, context) {
            if (!isInitialized) {
                element.style.opacity = 0;
                Velocity(element, {
                    opacity: 1
                });
            }
        };

        vm.checkCPF = function(strCPF) {
            var Soma;
            var Resto;
            Soma = 0;

            //strCPF  = RetiraCaracteresInvalidos(strCPF,11);
            if (strCPF == "00000000000")
                return false;

            for (i = 1; i <= 9; i++)
                Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);

            Resto = (Soma * 10) % 11;
            if ((Resto == 10) || (Resto == 11))
                Resto = 0;

            if (Resto != parseInt(strCPF.substring(9, 10)))
                return false;
            Soma = 0;

            for (i = 1; i <= 10; i++)
                Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
            Resto = (Soma * 10) % 11;

            if ((Resto == 10) || (Resto == 11))
                Resto = 0;

            if (Resto != parseInt(strCPF.substring(10, 11)))
                return false;

            return true;
        };

        this.preSignUp = function(e) {
            e.preventDefault();
            // remover o uso do localStorage
            localStorage.pre_sign_up = JSON.stringify({
                nome: e.target.nome.value,
                email: e.target.email.value,
                password: e.target.password.value,
                cpf: e.target.cpf.value
            });
            m.route('/abrir-conta/contaporemail');
        };

        this.tryLogin = function(e) {
            e.preventDefault();
            var token, email, password = "";

            email = e.target.email.value;
            password = e.target.password.value;
            Auth.login(email, password)
                .then(function(token) {
                    userModel.saveLogin({
                            'token': token
                        })
                        .then(function(user) {
                            m.module(document.getElementById('header'), headerM);
							var cookie_defaults = { domain : '.' + document.domain, expires: Infinity };
                            Cookies.set('logged_in', 'true', cookie_defaults);
                            Cookies.set('user_email', user.email, cookie_defaults);
                            Cookies.set('user_photourl', user.photo_url, cookie_defaults);
                            Cookies.set('user_setor', user.sector, cookie_defaults);
                            Cookies.set('user_nome', user.nome, cookie_defaults);

							if (m.route.param("back_to") && Cookies.get('logged_in')) {
                                var back_to = m.route.param("back_to");
                                if (back_to.indexOf('blog')) {
                                    window.location = back_to;
                                } else {
								    m.route(back_to);
                                }
							} else {
								if (phaseModel.getCurrentActive() == 1) {
                            		m.route('/vote');
								} else {
									m.route("/");
								}
							}
                        });

                }, function(err) {
                    vm.mensagemLoginValidacao(m(".alert.alert-error", {
                        config: vm.fadesIn
                    }, "Email ou senha inválidos"));
                });
        };
        this.vm = vm;
    },

    view: function(ctrl) {
        return [
            m('.container', [
                m('.pure-g.login-signup-box', [
                    m('.pure-u-1', [
                        m('h2.section-title', 'cadastre-se e participe'),
                        m('.baloon-bg.yellow'),
                    ]),
                    m('.pure-form', [
                        m('.pure-u-1.pure-u-sm-12-24.signup-box', [
                            m('h3.subsection-title', 'Cadastro'),
                            m('form.pure-form', {
                                    onsubmit: ctrl.preSignUp.bind(ctrl)
                                },
                                ctrl.vm.mensagemValidacao(),
                                m('input[required][type=text][name=nome][placeholder=Nome]'),
                                m('input[required][type=email][name=email][placeholder=Email]'),
                                m('input[required][maxlength=14][type=text][name=cpf][placeholder=CPF (apenas números)]', {
                                    onchange: m.withAttr('value', ctrl.vm.validateCPF),
                                    onkeypress: m.withAttr('value', ctrl.vm.cpfMask),
                                    value: ctrl.vm.cpf()
                                }),
                                m('input[required][type=password][name=password][placeholder=Senha]'),
                                m('button.pure-button', {
                                    type: 'submit'
                                }, 'Cadastrar')
                            ),
                        ]),

                        m('.pure-u-1.pure-u-sm-12-24.login-box', (ctrl.vm.visibleForgotPassword ? [
                            m('h3.subsection-title', 'esqueci senha'),
                            m('form.pure-form', {
                                    onsubmit: ctrl.resetPassword.bind(ctrl)
                                },
                                ctrl.vm.mensagemResetValidacao(),
                                m('p', 'Preencha abaixo seu e-mail e confira sua caixa de entrada:'),
                                m('input[required][type=email][name=reset_email][placeholder=Email]'),
                                m('button.pure-button', {
                                    type: 'submit'
                                }, 'pedir nova senha'),
                                m('a.pure-button[href=#]', {onclick: ctrl.vm.toggleForgotPassword}, 'Cancelar')
                            )
                        ] : [
                            m('h3.subsection-title', 'Entrar'),
                            fb_button_login(),
                            m('form.pure-form', {
                                    onsubmit: ctrl.tryLogin.bind(ctrl)
                                },
                                ctrl.vm.mensagemLoginValidacao(),
                                m('input[required][type=email][name=email][placeholder=Email]'),
                                m('input[required][type=password][name=password][placeholder=Senha]'),
                                m('button.pure-button', {
                                    type: 'submit'
                                }, 'Entrar'),
                                m('a[href=#]', {onclick: ctrl.vm.toggleForgotPassword}, 'Esqueceu sua senha?')
                            ),
                         ]))
                    ])
                ])
            ])
        ];
    }
};
