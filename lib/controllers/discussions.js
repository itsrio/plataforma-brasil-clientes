var m = require('mithril');
var c = require('config');
var _ = require('underscore');

var userModel = require('models/user');
var themeModel = require('models/theme');
var discussionModel = require('models/discussion');

var discussion_page = require('views/discussions');

var Discussions = module.exports = {
    controller: function() {
        var ctrl = this;
        var r = [];

        ctrl.theme_id = m.prop(parseInt(m.route.param("id")));
        ctrl.theme_active = m.prop(themeModel.get(ctrl.theme_id()));
        ctrl.list_discussions = m.prop(r);
        ctrl.is_visible_form_discussion = m.prop(false);
        ctrl.is_user_loggedin = m.prop(false);
        ctrl.current_user = m.prop({});
        ctrl.model = {
            'title': m.prop(''),
            'description': m.prop(''),
            'categories': m.prop([]),
            'theme_id': ctrl.theme_id,
            'message': m.prop([])
        };

        if (localStorage.token !== undefined) {
            userModel.getUser().then(ctrl.current_user);

            ctrl.is_user_loggedin(true);
        }

        this.toggle_form_discussion = function (ctrl, e) {
            e.preventDefault();

            if (ctrl.is_visible_form_discussion()) {
                ctrl.is_visible_form_discussion(false);
            } else {
                ctrl.is_visible_form_discussion(true);
            }
            //prevent click
            return false;
        };

        this.save_discussion = discussionModel.save_discussion;

        this.toggle_checkbox = function (ctrl, e) {
            var cls = e.currentTarget.className;
            var cats = ctrl.model.categories();
            var cat_name = e.currentTarget.getAttribute('data-value');

            if (cls.match(/selected/gi) !== null) {
                cats = _.filter(cats,
                                function(data) { 
                                    if (data.name == cat_name) {
                                        return data;
                                    }
                                    return false; 
                                });
                ctrl.model.categories(cats);
                e.currentTarget.className = cls.replace(/selected/gi, '');
            } else {
                e.currentTarget.className += ' selected';
                cats.push({ 'name': cat_name });
                ctrl.model.categories(cats);
            }
            return false;
        };

        this.setCategory = function (ctrl, e) {
            m.route('/fase-2-discutir-tema/'+ctrl.theme_active().id+'?categoria='+e.currentTarget.getAttribute('data-slug'));
            return false;
        };

        ctrl.order = m.prop(m.route.param('ordenar'));
        ctrl.category = m.prop(m.route.param('categoria'));
        ctrl.isLoading = m.prop(true);
        ctrl.total_records = m.prop(0);
        discussionModel.get_all(ctrl.theme_id(), ctrl.order(), ctrl.category()).then(function (data) {
            ctrl.isLoading(false);
            if (data !== null) {
                ctrl.list_discussions(data);
                ctrl.total_records(discussionModel.total_records());
            }
        });
        ctrl.get_more_discussions = discussionModel.get_more_discussions;
    },

    view: function(ctrl) {
        return [
            m('.discussions',
                discussion_page(ctrl)
            )
        ];
    }
};
