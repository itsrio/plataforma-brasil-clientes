/*jshint expr: true */
/*global describe, it */

var should = require('should');
var login = require('../../lib/modules/login');

describe('login', function () {

  describe('controller', function () {

    it('should be a function', function () {
      login.controller.should.be.a.function;
    });
/*
    it('should return an object', function () {
      login.controller().should.be.a.object;
    });
*/
  });

  describe('view', function () {

    it('should be a function', function () {
      login.view.should.be.a.function;
    });
/*
    it('should return an object', function () {
      login.view().should.be.a.object;
    });
*/
  });

});
