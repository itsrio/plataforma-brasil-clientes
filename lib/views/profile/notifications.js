var m = require('mithril');
var top_header = require('views/profile/top_header');
var divisor = require('views/profile/divisor');
var badge_user = require('views/common/badge_user');
var moment = require('moment');

var notification = function(ctrl, user, icon, action_desc, content, date, link, opened) {

    var class_opened = '.nao-lido';
    if (opened) {
        class_opened = '.lido';
    }
    return [
		m('.pure-g.notification-list.profile-info-notification'+class_opened,[
			m('.profile-info.pure-u-sm-2-24.pure-u-1-6',[
				badge_user(user, 40),
			]),
			m('.notification-icon.pure-u-sm-2-24.pure-u-2-24',
				m('h2.icon-'+icon)
			),
			m('.pure-u-sm-20-24.pure-u-18-24.notification-post',[
				m('strong.notification-name', user.nome + ' '),
				m('span.notification-action', action_desc + ' '),
				m('a.notification-place[href='+link+']', { config:m.route }, content),
				m('.notification-date', m('label',date))
			])
		])
	];
};

var notifications = function (ctrl) {
    return [
		m('.pure-u-1.pure-u-sm-2-5', divisor('circle-area-number',ctrl.total_notifications(),'Notificações')),
		m('.pure-u-1.pure-u-sm-3-5', 
			ctrl.model().map(function (v) {
				var u = {
					id: v.from_user_id,
					nome: v.from_user_name,
					photo_url: v.from_user_photo_url, 
					sector: v.from_user_sector
				};
				var n = {};
				n.when = moment(v.created_at).fromNow();
				n.content = v.discussion.title || 'sem título';
				n.link = '/fase-2-comentar-discussao/' + v.discussion.id  + '?leu_notificacao='+v.id;

				if (v.template_name === 'new_discussion_like') {
					n.icon        = 'notificacao-gostei';
					n.action_desc = 'gostou da sua discussão';
				} else if (v.template_name === 'new_discussion_dislike') {
					n.icon        = 'notificacao-nao-gostei';
					n.action_desc = 'não gostou da sua discussão';
				} else if (v.template_name === 'new_comment') {
					n.icon        = 'notificacao';
					n.action_desc = 'comentou na sua discussão';
				} else if (v.template_name === 'new_discussion_comment_like') {
					n.icon        = 'notificacao-gostei';
					n.action_desc = 'gostou do seu comentário';
					n.content     = v.discussion_comment_resource.description;
					n.link = '/fase-2-comentar-discussao/' + v.discussion_comment_resource.discussion_id + '?comentario_id=' + v.discussion_comment_resource.id + '&leu_notificacao='+v.id;
				} else if (v.template_name === 'new_discussion_comment_dislike') {
					n.icon        = 'notificacao-nao-gostei';
					n.action_desc = 'não gostou do seu comentário';
					n.content     = v.discussion_comment_resource.description;
					n.link = '/fase-2-comentar-discussao/' + v.discussion_comment_resource.discussion_id + '?comentario_id=' + v.discussion_comment_resource.id + '&leu_notificacao='+v.id;
				}
				return notification(ctrl, u, n.icon, n.action_desc, n.content, n.when, n.link, v.opened);
			})
		)
	];
};

module.exports = notifications;
