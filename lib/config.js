var config = {};

config.getAppFacebookId = function() {
    if (this.getEnv() == 'DEV') {
        return "815230245210441";
    }

    return "793593654040767";
};

config.getHost = function() {
    if (this.getEnv() == 'DEV') {
        return "http://localhost:8000";
    } else if (this.getEnv() == 'TEST') {
        return "https://testes.plataformabrasil.org.br";
    } else {
        return "https://plataformabrasil.org.br";
    }
};

config.getApiHost = function() {
    if (this.getEnv() == 'DEV') {
        return "http://localhost:3001";
        // return "https://api-testes.plataformabrasil.org.br";
    } else if (this.getEnv() == 'TEST') {
        return "https://api-testes.plataformabrasil.org.br";
    } else {
        return "https://api.plataformabrasil.org.br";
    }
};

config.getEnv = function() {
    if (location.hostname.indexOf("local") > -1) {
        return "DEV";
    } else if (location.hostname.indexOf("testes") > -1) {
        return "TEST";
    } else {
        return "PROD";
    }
};

module.exports = config;
